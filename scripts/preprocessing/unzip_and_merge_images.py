# unzip_and_merge_images.py
# Use this script
#   - to unzip files (paciente1.zip, paciente2.zip, ...)
#   - to convert DICOM files to JPG files
#   - to put all JPG images in a single directory
# 
# Run
# $ python unzip_and_merge_images.py -i /media/data/datasets/inr_dicom -o /media/data/datasets/inr_dicom/images -n paciente_
# $ python unzip_and_merge_images.py -i /home/auraham/Desktop/sandbox -o /home/auraham/Desktop/sandbox/images -n paciente_
# $ python unzip_and_merge_images.py -p /home/acamacho/inr_dicom_part_2/auraham/expedientes2605 -n paciente_

import os
import glob
from zipfile import ZipFile
from dicom2jpg import dicom2jpg
import shutil
import argparse

def copy_images(src_path, dst_path):
    
    # get list of all JPG images in src_path
    pattern = os.path.join(src_path, "*.jpg")
    img_filepaths = glob.glob(pattern)

    # create dst_path
    if not os.path.isdir(dst_path):
        os.mkdir(dst_path)

    # copy images to dst_path
    for src_filepath in img_filepaths:
        src_filename = os.path.basename(src_filepath)
        dst_filepath = os.path.join(dst_path, src_filename)
        shutil.copyfile(src_filepath, dst_filepath)
        print("copy: %s" % dst_filepath)


if __name__ == "__main__":

    # parse input
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input_path", required=True, help="Path of zip files (input)")
    ap.add_argument("-o", "--output_path", required=True, help="Path of jpg images (output)")
    args = vars(ap.parse_args())

    # get list of zip files
    input_path = args["input_path"]
    pattern = os.path.join(input_path, "paciente*.zip")
    zip_filepaths = glob.glob(pattern)

    for zip_filepath in zip_filepaths:

        # define paths
        basename = os.path.splitext(os.path.basename(zip_filepath))[0]      # paciente1
        src_path = os.path.join(input_path, basename)                       # input_path/paciente1
        dst_path = args["output_path"]
        
        # unzip
        with ZipFile(zip_filepath, "r") as zip_ref:
            zip_ref.extractall(input_path)

        # convert src_path/dicom files to src_path/jpg files
        prefix = basename + "_"
        dicom2jpg(src_path, prefix)

        # copy src_path/*.jpg into dst_path
        copy_images(src_path, dst_path)
        