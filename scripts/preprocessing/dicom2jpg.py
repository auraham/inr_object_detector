# dicom2jpg.py
# Run
# python dicom2jpg.py -p path
#
# Example
# python dicom2jpg.py -p /media/data/datasets/inr_dicom/paciente1/ -n paciente_1_
# python dicom2jpg.py -p /media/data/datasets/inr_dicom/paciente2/ -n paciente_2_
# python dicom2jpg.py -p /media/data/datasets/inr_dicom/paciente3/ -n paciente_3_
import os
import pydicom
from pydicom.pixel_data_handlers.util import apply_modality_lut
import torch
import numpy as np
import matplotlib.pyplot as plt
import argparse

def dicom2jpg(input_path, prefix):

    # get list of DICOM files (they do not have extension)
    filepaths = [os.path.join(input_path, fname) 
                for fname in os.listdir(input_path)
                if os.path.splitext(fname)[-1] == ""]

    # read files
    for filepath in filepaths:

        # read slice
        img = pydicom.dcmread(filepath)

        # convert to Hounsfield Units (HU)
        data = img.pixel_array.copy()
        data = apply_modality_lut(data, img)

        # save JPG
        basename = os.path.basename(filepath)
        output_filename = prefix + basename + ".jpg"
        output_filepath = os.path.join(input_path, output_filename)
        plt.imsave(output_filepath, data, cmap="gist_gray")
        print(output_filepath)


if __name__ == "__main__":

    # parse input
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--path", required=True, help="Path of images")
    ap.add_argument("-n", "--name", required=True, help="Prefix")
    args = vars(ap.parse_args())

    input_path = args["path"]
    prefix = args["name"]
    dicom2jpg(input_path, prefix)
   
