# Pre-trained models

This directory contains pre-trained models (`tar.gz` files) from the [Model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md).

