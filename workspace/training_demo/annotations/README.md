# Annotations

This directory contains:

- `label_map.pbtxt`: This file specifies the names of the clases.
- `train.record`: Training images in TF Record format.
- `test.record`: Testing images in TF Record format.

