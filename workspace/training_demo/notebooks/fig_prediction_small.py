# fig_prediction+small.py
# Create a plot with the predictions of the model
import numpy as np
from PIL import Image
import imageio
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import glob
import os
import tensorflow as tf
from object_detection.utils import visualization_utils as viz_utils

# do not remove
import matplotlib
matplotlib.use("tkagg")

def load_image_into_numpy_array(path):
    """
    Return image as np.array
    """
    return np.array(Image.open(path))


def get_bbox_list(xml_path):
    """
    Return np.array of bounding boxes.
    Each row is a bounding box.
    """

    bbox_list = []
    tree = ET.parse(xml_path)
    root = tree.getroot()

    for member in root.findall("object"):
        
        xmin = int(member[4][0].text)
        ymin = int(member[4][1].text)
        xmax = int(member[4][2].text)
        ymax = int(member[4][3].text)

        bbox_coords = (ymin, xmin, ymax, xmax) 

        bbox_list.append(bbox_coords) 

    return np.array(bbox_list)


if __name__ == "__main__":

    # ---- load model ----
    print("Loading model...")
    tf.keras.backend.clear_session()
    SAVED_MODEL_PATH = "../exported_models/centernet_hg104_512x512_coco17_tpu-8/saved_model"
    detect_fn = tf.saved_model.load(SAVED_MODEL_PATH)
    print("Done!")

    category_index = {
        1: {"id": 1, "name": "masa"},
    }

    image_dir = "../images/test"
    IMAGES = sorted(glob.glob(image_dir + "/*.jpg"))[:3]

    #fig, axes = plt.subplots(1, 3, figsize=(9.5, 3.8))
    fig, axes = plt.subplots(1, 3, figsize=(8.4, 3))
    
    for i in range(3):

        image_path = IMAGES[i]
        ax = axes.flat[i]

        # jpg file
        image_jpg = os.path.basename(image_path)

        # xml file
        basename = os.path.splitext(os.path.basename(image_path))[0]
        dirname = os.path.dirname(image_path)
        image_xml = os.path.join(dirname, basename + ".xml")

        # ------- groundtruth -------

        # get bounding boxes
        bboxes = get_bbox_list(image_xml)
        n = len(bboxes)      
        bboxes_classes = np.ones(n, dtype=int)          # aqui asumimos que solo hay una clase
        bboxes_scores = np.ones(n)                      # aqui asumimos que el score de todos los bounding boxes es 1

        # load original image
        img_np = load_image_into_numpy_array(image_path)

        # draw bounding boxes
        img_np_with_detections = img_np.copy()
        viz_utils.visualize_boxes_and_labels_on_image_array(
            img_np_with_detections,
            bboxes,
            bboxes_classes,
            None, #bboxes_scores,  # None: asume que los bboxes son groundtruth
            category_index,
            use_normalized_coordinates=False,
            max_boxes_to_draw=200,
            min_score_thresh=.40,
            agnostic_mode=False,
            groundtruth_box_visualization_color="Red"
            )

        # ------- predictions -------

        # predictions
        input_tensor = np.expand_dims(img_np.copy(), 0)
        detections = detect_fn(input_tensor)

        # draw bounding boxes
        viz_utils.visualize_boxes_and_labels_on_image_array(
            img_np_with_detections,                             # use the same img_np_with_detections as above
            detections["detection_boxes"][0].numpy(),
            detections["detection_classes"][0].numpy().astype(np.int32),
            detections["detection_scores"][0].numpy(),
            category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=.40,
            agnostic_mode=False,
            )

        # plot image with bounding boxes
        ax.imshow(img_np_with_detections)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_title(image_jpg, size=8)

    # adjust margins
    plt.subplots_adjust(bottom=0.04, 
                        top=0.94,
                        left=0.02,
                        right=0.98,
                        wspace=0.25,
                        hspace=0.35)
    
    # save figure
    figname = "fig_prediction_small.jpg"
    #fig.savefig(figname, dpi=300, facecolor="#c0c0c0")
    fig.savefig(figname, dpi=300)
    print(figname)
    
    plt.show()
    

