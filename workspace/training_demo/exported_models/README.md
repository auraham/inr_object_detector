# Exported models

This directory contains exported models. A exported model looks like this:

```
exported_models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 2
├── checkpoint
│   ├── checkpoint
│   ├── ckpt-0.data-00000-of-00001
│   └── ckpt-0.index
├── pipeline.config
└── saved_model
    ├── assets
    ├── saved_model.pb
    └── variables
```

See the [documentation](https://www.tensorflow.org/guide/saved_model#the_savedmodel_format_on_disk) for more details.

