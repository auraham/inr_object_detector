# Models

This directory contains models after training. A model is a directory with these files:

```
models/centernet_hg104_512x512_coco17_tpu-8 $ tree -L 1
.
├── checkpoint                             # text file that defines the last n checkpoints
├── ckpt-NUMBER.data-00000-of-00001        # model file
├── ckpt-NUMBER.index                      # model file
└── pipeline.config                        # config file for configuring the training process
```

