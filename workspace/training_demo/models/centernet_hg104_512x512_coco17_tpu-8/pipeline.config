# Check
# https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/configuring_jobs.md
# to configure this file
# 
# At high level, the config file has five parts
# 
#   model               This defines what type of model will be trained 
#                       (e.g., meta-arquitecture, feature extractor)
#   train_config        This defines what parameters should be used to train model parameters
#                       (e.g., SGD parameters, input preprocessing/data augmentation, and feature extractor initialization values)
#   
#   train_input_reader  This defines what dataset the model should be trained on
#   
#   eval_config         This defines what set of metrics will be reported for evaluation
#   
#   eval_input_reader   This defines what dataset the model will be evaluated on.
#                       Typically this should be different than the training input dataset.

model {
  center_net {
    # Set number of classes
    num_classes: 1 
    feature_extractor {
      type: "hourglass_104"
      channel_means: 104.01361846923828
      channel_means: 114.03422546386719
      channel_means: 119.91659545898438
      channel_stds: 73.60276794433594
      channel_stds: 69.89082336425781
      channel_stds: 70.91507720947266
      bgr_ordering: true
    }
    image_resizer {
      keep_aspect_ratio_resizer {
        min_dimension: 512
        max_dimension: 512
        pad_to_max_dimension: true
      }
    }
    object_detection_task {
      task_loss_weight: 1.0
      offset_loss_weight: 1.0
      scale_loss_weight: 0.10000000149011612
      localization_loss {
        l1_localization_loss {
        }
      }
    }
    object_center_params {
      object_center_loss_weight: 1.0
      classification_loss {
        penalty_reduced_logistic_focal_loss {
          alpha: 2.0
          beta: 4.0
        }
      }
      min_box_overlap_iou: 0.699999988079071
      max_box_predictions: 100
    }
  }
}

train_config {
  # Increase/Decresase batch_size depending on the available memory
  batch_size: 1 
  data_augmentation_options {
    random_horizontal_flip {
    }
  }
  data_augmentation_options {
    random_crop_image {
      min_aspect_ratio: 0.5
      max_aspect_ratio: 1.7000000476837158
      random_coef: 0.25
    }
  }
  data_augmentation_options {
    random_adjust_hue {
    }
  }
  data_augmentation_options {
    random_adjust_contrast {
    }
  }
  data_augmentation_options {
    random_adjust_saturation {
    }
  }
  data_augmentation_options {
    random_adjust_brightness {
    }
  }
  data_augmentation_options {
    random_absolute_pad_image {
      max_height_padding: 200
      max_width_padding: 200
      pad_color: 0.0
      pad_color: 0.0
      pad_color: 0.0
    }
  }
  optimizer {
    adam_optimizer {
      learning_rate {
        manual_step_learning_rate {
          initial_learning_rate: 0.0010000000474974513
          schedule {
            step: 90000
            learning_rate: 9.999999747378752e-05
          }
          schedule {
            step: 120000
            learning_rate: 9.999999747378752e-06
          }
        }
      }
      epsilon: 1.0000000116860974e-07
    }
    use_moving_average: false
  }
  
  # Path to the checkpoint of pre-trained model
  fine_tune_checkpoint: "pretrained_models/centernet_hg104_512x512_coco17_tpu-8/checkpoint/ckpt-0"
  
  # Number of training steps
  num_steps: 30000

  max_number_of_boxes: 100
  unpad_groundtruth_tensors: false

  # Set this to "detection" since we want to be training the full detection model
  fine_tune_checkpoint_type: "detection"

  fine_tune_checkpoint_version: V2
}

train_input_reader: {
  # Path to label map file
  label_map_path: "annotations/label_map.pbtxt"
  tf_record_input_reader {
    # Path to training TFRecord file
    input_path: "annotations/train.record"
  }
}

eval_config {
  metrics_set: "coco_detection_metrics"
  use_moving_averages: false
  batch_size: 1
}

eval_input_reader: {
  # Path to label map file
  label_map_path: "annotations/label_map.pbtxt"
  shuffle: false
  num_epochs: 1
  tf_record_input_reader {
    # Path to testing TFRecord file
    input_path: "annotations/test.record"
  }
}
