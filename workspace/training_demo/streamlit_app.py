# streamlit_app.py
# Run
# streamlit run streamlit_app.py
# streamlit run streamlit_app.py --server.port 6006
import streamlit as st

# --- Tensorflow ----------------------------------------------
# This part loads the model

# ----------------------------------------------
# Initial setup
# ----------------------------------------------

import io
import os
import scipy.misc
import numpy as np
import six
import time
import glob

from six import BytesIO

import matplotlib
import matplotlib.pyplot as plt
from PIL import Image

import tensorflow as tf
from object_detection.utils import visualization_utils as viz_utils
from object_detection.utils import label_map_util


# ----------------------------------------------
# Load labels
# ----------------------------------------------

LABEL_FILENAME = "label_map.pbtxt"
PATH_TO_LABELS = os.path.join("annotations", LABEL_FILENAME)


# ----------------------------------------------
# Load model
# ----------------------------------------------

@st.cache(suppress_st_warning=True, allow_output_mutation=True)
def load_model():
    """
    Load model into the cache
    """

    detect_fn = None
    tf.keras.backend.clear_session()

    with st.spinner("Loading model for the first time..."):

        SAVED_MODEL_PATH = "/home/auraham/git/tf_object_detection_api/workspace/training_demo/exported_models/centernet_hg104_512x512_coco17_tpu-8/saved_model"
        detect_fn = tf.saved_model.load(SAVED_MODEL_PATH)

    return detect_fn

# ----------------------------------------------
# Load label map data (for plotting)
# ----------------------------------------------

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                    use_display_name=True)

# ----------------------------------------------
# Test model
# ----------------------------------------------

def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.

    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.

    Args:
    path: a file path (this can be local or on colossus)

    Returns:
    uint8 numpy array with shape (img_height, img_width, 3)
    """
    img_data = tf.io.gfile.GFile(path, 'rb').read()
    image = Image.open(BytesIO(img_data))
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

# --- Streamlit -----------------------------------------------
# This part renders the page

import os
import glob

def file_selector(path="images/test", default="images/test/paciente1_42.jpg"):
    filenames = os.listdir(path)
    pattern = os.path.join(path, "*.jpg")
    filepaths = glob.glob(pattern)
    selected_filepath = st.selectbox("Select a file", filepaths)
    return selected_filepath

# TODO agregar un combo y un area para subir una imagen

st.set_page_config(
    #layout="wide",
    page_title="Object Detection Demo"
)

st.write("""
# Object Detection Demo
""")

# upload image or...

#uploaded_file = st.file_uploader("Choose a file")

# ... select a file

#input_image = "test_images/image-1.jpg"
input_image = file_selector()

st.write("**Image** " +  input_image)

st.image(input_image)

if st.button("Detect objects"):

    with st.spinner("Detecting..."):

        # --- load model ---

        detect_fn = load_model()

        # --- detection ---

        image_np = load_image_into_numpy_array(input_image)
        input_tensor = np.expand_dims(image_np, 0)
        detections = detect_fn(input_tensor)

        image_np_with_detections = image_np.copy()
        viz_utils.visualize_boxes_and_labels_on_image_array(
            image_np_with_detections,
            detections['detection_boxes'][0].numpy(),
            detections['detection_classes'][0].numpy().astype(np.int32),
            detections['detection_scores'][0].numpy(),
            category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=.40,
            agnostic_mode=False)

        # output name
        basename = os.path.splitext(os.path.basename(input_image))[0]
        output_image = os.path.join("output", basename + ".jpg")
        
        # save figure
        # use these settings: figsize=(5.12, 5.12) and dpi=100
        fig = plt.figure(figsize=(5.12, 5.12))
        ax = plt.Axes(fig, [0, 0, 1, 1])
        ax.set_axis_off()
        fig.add_axes(ax)
        plt.imshow(image_np_with_detections, aspect="auto")
        plt.savefig(output_image, dpi=100)
        plt.close(fig)
        print(output_image)

        # show image in app
        st.image(output_image)

        # --- detection ---

    st.success("Done!")
    